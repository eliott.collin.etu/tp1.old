let data = [
    {
        name: 'Regina',
        base: 'tomate',
        price_small: 6.5,
        price_large: 9.95,
        image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
    },
    {
        name: 'Napolitaine',
        base: 'tomate',
        price_small: 6.5,
        price_large: 8.95,
        image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
    },
    {
        name: 'Spicy',
        base: 'crème',
        price_small: 5.5,
        price_large: 8,
        image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
    }
];
/*
data.sort(function (a, b) { // sort by name
    if(a.name == b.name) return 0;
    if(a.name > b.name) return 1;
    else return -1;
});
*/

/*
data.sort(function(a, b) { // sort by small price
    if((a.price_small - b.price_small) == 0) return a.price_large - b.price_large;
    else return a.price_small - b.price_small;
});
*/

/*
data = data.filter(pizza => pizza.base == 'tomate'); // on enlève toute les pizza qui n'on pas une base 'tomate'
data = data.filter(pizza => pizza.price_small < 6); // on enlève toute les pizza dont le small price est < 6€
*/
/*
data = data.filter(function({name}) { // on garde toute les pizza qui contiennent  2 fois la lettre 'i'
    let nbI = 0;
    for(let i = 0; i < name.length; i++) {
        if(name[i] == 'i') nbI ++;
    }
    return nbI == 2;
}); 
*/
let html = '';
data.forEach(function({image, name, price_small, price_large, }){
    let url = image;
    let img = `<img src="${url}"></img>`;
    let balise = '<section>' +
        '<h4>' + name + '</h4>' +
        '<ul>' +
        '<li> Prix petit format :' + price_small + ' € </li>' +
        '<li> Prix grand format :' + price_large + ' € </li>' +
        '</ul>' +
        '</section>';
    let link = `<a href="${url}">${img}${balise}</a>`;
    html += `<article class="pizzaThumbnail">${link}</article>`;
});
// console.log(html);
document.querySelector('.pageContent').innerHTML = html;