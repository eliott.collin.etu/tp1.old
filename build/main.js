"use strict";

/*
const name = 'Regina';
let url = 'images/' + name.toLowerCase() + '.jpg';
let img = `<img src="${url}"></img>`;
let balise = '<section>' + name + '</section>';
let link = `<a href="${url}"> ${img} ${balise} </a>`;
let html = `<article class="pizzaThumbnail">${link}</article>`;
*/

/*
let html = '';
function image(name) {
    let url = 'images/' + name.toLowerCase() + '.jpg';
    let img = `<img src="${url}"></img>`;
    let balise = '<section>' + name + '</section>';
    let link = `<a href="${url}"> ${img} ${balise} </a>`;
    html += `<article class="pizzaThumbnail">${link}</article>`;
}

const data = ['Regina', 'Napolitaine', 'Spicy'];

data.forEach(element => image(element));
document.querySelector('.pageContent').innerHTML = html;
*/

/*
function image2(name) {
    let url = 'images/' + name.toLowerCase() + '.jpg';
    let img = `<img src="${url}"></img>`;
    let balise = '<section>' + name + '</section>';
    let link = `<a href="${url}"> ${img} ${balise} </a>`;
    return `<article class="pizzaThumbnail">${link}</article>`;
}

const data = ['Regina', 'Napolitaine', 'Spicy'];
const mapData = data.map(element => image2(element));
const result = mapData.join('');
document.querySelector('.pageContent').innerHTML = result;
*/

/*
let html = '';
for(let i = 0; i < data.length; i++) {
    let url = 'images/' + data[i].toLowerCase() + '.jpg';
    let img = `<img src="${url}"></img>`;
    let balise = '<section>' + data[i] + '</section>';
    let link = `<a href="${url}"> ${img} ${balise} </a>`;
    html = html + `<article class="pizzaThumbnail">${link}</article>`;

}
console.log(html);
document.querySelector('.pageContent').innerHTML = html;
*/
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
var html = '';

for (var i = 0; i < data.length; i++) {
  var url = data[i].image;
  var img = "<img src=\"".concat(url, "\"></img>");
  var balise = '<section>' + '<h4>' + data[i].name + '</h4>' + '<ul>' + '<li> Prix petit format :' + data[i].price_large + ' € </li>' + '<li> Prix grand format :' + data[i].price_small + ' € </li>' + '</ul>' + '</section>';
  var link = "<a href=\"".concat(url, "\">").concat(img).concat(balise, "</a>");
  html = html + "<article class=\"pizzaThumbnail\">".concat(link, "</article>");
}

console.log(html);
document.querySelector('.pageContent').innerHTML = html;